# Mihhail Sidorin CV

Firebase app to host live CV


Production deployment on [https://mihhail-sidorin.firebaseapp.com
](https://mihhail-sidorin.firebaseapp.com)

## Project setup
Setup ruby environment and install `.ruby-version` ruby

```
rake init
```

## Deployment

```
rake deploy
```
